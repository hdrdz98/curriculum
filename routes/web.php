<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');

Route::get('login', 'LoginController@showForm')->name('login');
Route::get('logout', 'LoginController@logout')->name('logout');

Route::get('tmp/{num}', function($num) {
    return view('cv.templates.template'.$num);
})->name('tmp');

Route::get('mycvs', 'CVController@myCVs')->name('mycvs');

Route::get('cv/new', 'CVController@createNewCVAndRedirect')->name('cvs.new');

Route::prefix('cv/{id}')->group(function () {
    Route::get('/', 'CVController@viewCV')->name('cv.simple.view');
    Route::get('download/{template}', 'CVController@downloadCV')->name('cv.download');
    Route::get('new', 'CVController@showNewForm')->name('cv.new');
    Route::get('edit', 'CVController@showEditArea')->name('cv.edit.form');
    Route::get('edit/{template}', 'CVController@showEditAreaWTemplate')->name('cv.edit.form.temp');
    Route::get('view', 'CVController@showCV')->name('cv.view');
    Route::get('delete', 'CVController@delete')->name('cv.delete');
    Route::post('edit', 'CVController@editCV')->name('cv.edit');
    Route::post('editjson1', 'CVController@editCVJson1')->name('cv.editcvjson1');
    Route::post('editjson2', 'CVController@editCVJson2')->name('cv.editcvjson2');
    Route::post('editjson3', 'CVController@editCVJson3')->name('cv.editcvjson3');
    Route::post('editjson4', 'CVController@editCVJson4')->name('cv.editcvjson4');
    Route::post('editjson5', 'CVController@editcvjson5')->name('cv.editcvjson5');
});

Route::get('loginGithub', 'LoginController@redirectToGitHub')->name('github.login');
Route::get('loginGithub/callback', 'LoginController@handleGitHubCallback');

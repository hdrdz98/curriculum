@extends('templates/main')
@section('styles')
<style>

#main-title {
    font-size: 6rem;
    font-family: 'Acme', sans-serif;
}

#main-subtitle {
    font-size: 2rem;
}

</style>
@endsection
@section('content')

<h1 id="main-title" class="title">
    LET'S CV!
</h1>
<h2 id="main-subtitle" class="subtitle">
    Tool for easy cv generation
</h2>
<div class="cv-flex-cc">
    <a href="{{ route('login') }}" class="waves-effect waves-light btn-large indigo lighten-1 cv-flex-cc">
        Try now! <i class="fas fa-arrow-right" style="margin-left: 20px;"></i>
    </a>
</div>
<div class="cv-flex-cc" style="flex-flow: column; margin-top: 10px;">
    <p style="font-variant: small-caps; font-size: 14px;
        margin-top: 5px; color: #75787B;">Create a good CV</p>
</div>

<script>

</script>
@endsection
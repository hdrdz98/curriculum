@extends('../templates/main')

@section('styles')
<style>

#edit-title {
    font-size: 3rem;
    font-family: 'Alata', sans-serif;
}

#main-subtitle {
    font-size: 1.5rem;
}

#fillable-area {
    height: calc(100vh - 128px);
    background-color: white;
    width: 60%;
}

#pdf-area {
    height: calc(100vh - 128px);
    background-color: #363636;
    width: 40%;
}

#advance-tool {
    width: 40%;
}

.advance-dot {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    position: absolute;
    background-color: #dbdbdb/*#4a4a4a*/;
    bottom: 2%;
}

.datepicker-modal {
    height: 350px;
}

</style>
@endsection

@section('content')

<div style="display: flex;">
    <div id="pdf-area" style="display: flex; justify-content: center; align-items: center;">
        <div style="width: 80%; height: 100%; display: flex; justify-content: center; align-items: center; overflow: auto; background-color: white;">
            <iframe id="pdf-iframe" src="{{ route('tmp', $cv->template ? $cv->template : '1') }}" scrolling="yes" frameborder="0" 
                style="height: 100%; width: 100%; transform-origin: top left;"></iframe>
            <div style="position: absolute; bottom: 0; margin-bottom: 20px;">
                <a href="{{ route('cv.download', [$cv->id, 'template1']) }}" target="_blank" class="waves-effect waves-light btn-large" style="background-color: #1D4289">Download PDF!</a>
            </div>
            <div id="zoom-btn" style="position: absolute; top: 0; margin-top: 20px; left: 0%; 
                margin-left: 20px; background-color: #1D4289; width: 40px; height: 40px;
                display: flex; justify-content: center; align-items: center; color: white;
                border-radius: 50%; cursor: pointer; transition: all 0.5s">
                <i id="zoom-in" class="fas fa-search-plus"></i>
                <i id="zoom-out" class="fas fa-search-minus" style="display: none"></i>
            </div>
        </div>
    </div>
    <div id="fillable-area">
        <div id="fill-info" style="padding: 20px; height: calc(100vh - 128px - 72px); overflow-y: auto">
            <div id="fill-info-1">
                <h1 id="edit-title" class="title">
                    @if($cv->first_name) {{ $cv->first_name }} @endif @if($cv->lastname) {{ $cv->last_name }} @endif
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="cv-flex-cc" style="height: 100%; padding: 50px;">
                    <span class="file-cta" id="img-area" style="
                        background-size: cover; background-position: center;
                        height: 150px; width: 150px; padding: 0px;"></span>
                
                    <div class="card blue-grey darken-1" style="width: 50%; margin-left: 50px;">
                        <div class="card-content white-text">
                            <span class="card-title">Hi!</span>
                            <p>You can visit my CV from this URL</p>
                        </div>
                        <div class="card-action">
                            <a href="{{ route('cv.view', $cv->id) }}">{{ route('cv.view', $cv->id) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cv-flex-cc" >
            <div style="position: absolute; bottom: 0; margin-bottom: 20px;">
                <a href="{{ route('cv.simple.view', $cv->id) }}" target="_blank" class="waves-effect waves-light btn-large" style="background-color: #1D4289;">See CV!</a>
            </div>
        </div>
    </div>
</div>


<script>

var zoom = false;

$("#zoom-btn").click(function () {
    if (zoom == false) {
        $("#zoom-out").css('display', 'block');
        $("#zoom-in").css('display', 'none');
        $("#pdf-iframe").css('transform', 'scale(2)');
    } else {
        $("#zoom-out").css('display', 'none');
        $("#zoom-in").css('display', 'block');
        $("#pdf-iframe").css('transform', 'scale(1)');
    }
    zoom = !zoom;
});

$("#img-area").css({
    "background-image": @if($cv->photo) "url('{{ asset("$cv->photo") }}') @else "url('{{ Storage::url('users/profilepics/no-photo.jpg') }}') @endif"
});

</script>

@endsection
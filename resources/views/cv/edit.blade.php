@extends('../templates/main')

@section('styles')
<style>

#edit-title {
    font-size: 3rem;
    font-family: 'Alata', sans-serif;
}

#main-subtitle {
    font-size: 1.5rem;
}

#fillable-area {
    height: calc(100vh - 128px);
    background-color: white;
    width: 60%;
}

#pdf-area {
    height: calc(100vh - 128px);
    background-color: #363636;
    width: 40%;
}

#advance-tool {
    width: 40%;
}

.advance-dot {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    position: absolute;
    background-color: #dbdbdb/*#4a4a4a*/;
    bottom: 2%;
}

.datepicker-modal {
    height: 350px;
}

</style>
@endsection

@section('content')

<div style="display: flex;">
    <div id="fillable-area">
        <div id="fill-info" style="padding: 20px; height: calc(100vh - 128px - 72px); overflow-y: auto">
            <div id="fill-info-1">
                <h1 id="edit-title" class="title">
                    Personal Information
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="row" style="margin-bottom: 0;">
                    <div class="col s12">
                        <div class="row" style="margin-top: 20px; margin-bottom: 0px;">
                            <div class="input-field col s6">
                                <input id="icon_firstname" required type="text" class="input_v1 validate" 
                                    @if($cv->first_name) value="{{ $cv->first_name }}"
                                    @else @if($user) value="{{ explode(' ', $user->name)[0] }}" @endif @endif>
                                <label for="icon_firstname">First Name</label>
                            </div>
                            <div class="cv-flex-cc" style="float: right; width: 50%;">
                                <div class="file has-name is-boxed" style="float: right">
                                    <label class="file-label" style="width: 10rem; height: 10rem;">
                                        <input class="input_v1 file-input" id="file" type="file" name="image">
                                        <span class="file-cta" id="img-area" style="
                                            background-size: cover; background-position: center;
                                            height: 100%; padding: 0px;"></span>
                                        <span class="file-name" id="filename">
                                            Imagen de perfil
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="input-field col s6">
                                <input id="icon_lastname" required type="text" class="input_v1 validate" 
                                    @if($cv->last_name) value="{{ $cv->last_name }}"
                                    @else @if($user) value="{{ explode(' ', $user->name)[1] }}" @endif @endif>
                                <label for="icon_lastname">Last Name</label>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 0px;">
                            <div class="input-field col s6">
                                <i class="fas fa-envelope prefix"></i>
                                <input id="icon_email" type="tel" class="input_v1 validate" 
                                    @if($cv->email) value="{{ $cv->email }}"
                                    @else @if($user) value="{{ $user->email }}" @endif @endif>
                                <label for="icon_email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="fas fa-mobile-alt prefix"></i>
                                <input id="icon_telephone" type="tel" class="input_v1 validate"
                                    @if($cv->telephone) value="{{ $cv->telephone }}" @endif>
                                <label for="icon_telephone">Telephone</label>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 0;">
                            <div class="input-field col s4">
                                <i class="fab fa-facebook-square prefix"></i>
                                <input id="icon_facebook" type="tel" class="input_v1 validate"
                                    @if($cv->facebook) value="{{ $cv->facebook }}" @endif>
                                <label for="icon_facebook">Facebook</label>
                            </div>
                            <div class="input-field col s4">
                                <i class="fab fa-github-square prefix"></i>
                                <input id="icon_github" type="tel" class="input_v1 validate"
                                    @if($cv->github) value="{{ $cv->github }}" @endif>
                                <label for="icon_github">Github</label>
                            </div>
                            <div class="input-field col s4">
                                <i class="fab fa-linkedin prefix"></i>
                                <input id="icon_linkedin" type="tel" class="input_v1 validate"
                                    @if($cv->linkedin) value="{{ $cv->linkedin }}" @endif>
                                <label for="icon_linkedin">LinkedIn</label>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 0;">
                            <button class="input_v1 btn nextInfo waves-effect waves-light" style="background-color: #1D4289">Continue
                                <i class="fas fa-arrow-right right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="fill-info-2" style="display: none">
                <h1 id="edit-title" class="title">
                    Professional Experience
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="col s12">
                    <div id="laboral-container" style="margin-top: 20px;">
                        @if($cv->professional_experience)
                            @foreach($cv->professional_experience as $key => $enterprise)
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="input-field col s6">
                                        <i class="fas fa-building prefix"></i>
                                        <input id="icon_ent_name_{{$key}}" type="text" class="icon_ent_name input_v2 validate"
                                            @if($enterprise['name']) value="{{ $enterprise['name'] }}" @endif>
                                        <label for="icon_ent_name_{{$key}}">Enterprise</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <i class="fab fa-black-tie prefix"></i>
                                        <input id="icon_ent_job_title_{{$key}}" type="tel" class="icon_ent_job_title input_v2 validate"
                                            @if($enterprise['job_title']) value="{{ $enterprise['job_title'] }}" @endif>
                                        <label for="icon_ent_job_title_{{$key}}">Job Title</label>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="col s6">
                                        <div class="row" style="margin-bottom: 0px;">
                                            <div class="input-field col s12">
                                                <i class="fas fa-edit prefix"></i>
                                                <textarea id="icon_ent_description_{{$key}}" class="icon_ent_description input_v2 materialize-textarea" data-length="280" maxlength="280">@if($enterprise['description']) {{ $enterprise['description'] }}@endif</textarea>
                                                <label for="icon_ent_description_{{$key}}">Description</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 0px; width: 100%;">
                                        <div class="input-field col s2">
                                            <input id="icon_ent_start_{{$key}}" type="text" class="icon_ent_start input_v2 datepicker"
                                                @if($enterprise['start']) value="{{ $enterprise['start'] }}" @endif>
                                            <label for="icon_ent_start_{{$key}}">Start Date</label>
                                        </div>
                                        <div class="input-field col s2">
                                            <input id="icon_ent_end_{{$key}}" type="text" class="icon_ent_end input_v2 datepicker"
                                                @if($enterprise['end']) value="{{ $enterprise['end'] }}" @endif>
                                            <label for="icon_ent_end_{{$key}}">End Date</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="input-field col s6">
                                    <i class="fas fa-building prefix"></i>
                                    <input id="icon_ent_name_1" type="text" class="icon_ent_name input_v2 validate">
                                    <label for="icon_ent_name_1">Enterprise</label>
                                </div>
                                <div class="input-field col s6">
                                    <i class="fab fa-black-tie prefix"></i>
                                    <input id="icon_ent_job_title_1" type="tel" class="icon_ent_job_title input_v2 validate">
                                    <label for="icon_ent_job_title_1">Job Title</label>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="col s6">
                                    <div class="row" style="margin-bottom: 0px;">
                                        <div class="input-field col s12">
                                            <i class="fas fa-edit prefix"></i>
                                            <textarea id="icon_ent_description_1" class="icon_ent_description input_v2 materialize-textarea" data-length="280" maxlength="280"></textarea>
                                            <label for="icon_ent_description_1">Description</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 0px; width: 100%;">
                                    <div class="input-field col s2">
                                        <input id="icon_ent_start_1" type="text" class="icon_ent_start input_v2 datepicker">
                                        <label for="icon_ent_start_1">Start Date</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input id="icon_ent_end_1" type="text" class="icon_ent_end input_v2 datepicker">
                                        <label for="icon_ent_end_1">End Date</label>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div style="display: flex; justify-content: flex-start; align-items: center;">
                        <a id="laboral-add" class="btn-floating btn-large waves-effect waves-light btn-small">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="row" style="margin-bottom: 0;">
                        <button class="btn lastInfo waves-effect waves-light indigo lighten-4">Return
                            <i class="fas fa-arrow-left left"></i>
                        </button>
                        <button class="btn nextInfo waves-effect waves-light" style="background-color: #1D4289">Continue
                            <i class="fas fa-arrow-right right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div id="fill-info-3" style="display: none">
                <h1 id="edit-title" class="title">
                    Education
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="col s12">
                    <div id="education-container" style="margin-top: 20px;">
                        @if($cv->education)
                            @foreach($cv->education as $key => $education)
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="input-field col s6">
                                        <i class="fas fa-university prefix"></i>
                                        <input id="icon_ed_institute_{{$key}}" type="text" class="icon_ed_institute input_v3 validate"
                                            @if($education['institute']) value="{{ $education['institute'] }}" @endif>
                                        <label for="icon_ed_institute_{{$key}}">Institute</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <i class="fas fa-medal prefix"></i>
                                        <input id="icon_ed_degree_{{$key}}" type="tel" class="icon_ed_degree input_v3 validate"
                                            @if($education['degree']) value="{{ $education['degree'] }}" @endif>
                                        <label for="icon_ed_degree_{{$key}}">Degree</label>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="col s6">
                                        <div class="input-field col s12">
                                            <select id="icon_ed_study_type_{{$key}}" class="icon_ed_study_type input_v3">
                                                <option value="" disabled @if(!$education['study_type']) selected @endif>Choose your option</option>
                                                <option value="High School" @if($education['study_type'] && $education['study_type']=='High School') selected @endif>High School</option>
                                                <option value="Associate degree" @if($education['study_type'] && $education['study_type']=='Associate degree') selected @endif>Associate degree</option>
                                                <option value="Bachelor" @if($education['study_type'] && $education['study_type']=='Bachelor') selected @endif>Bachelor</option>
                                                <option value="Master" @if($education['study_type'] && $education['study_type']=='Master') selected @endif>Master</option>
                                                <option value="Doctorate" @if($education['study_type'] && $education['study_type']=='Doctorate') selected @endif>Doctorate</option>
                                            </select>
                                            <label>Study type</label>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">
                                        <i class="fas fa-graduation-cap prefix"></i>
                                        <input id="icon_ed_score_{{$key}}" type="tel" class="icon_ed_score input_v3 validate"
                                            @if($education['score']) value="{{ $education['score'] }}" @endif>
                                        <label for="icon_ed_score_{{$key}}">Score</label>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="input-field col s6">
                                    <i class="fas fa-university prefix"></i>
                                    <input id="icon_ed_institute_1" type="text" class="icon_ed_institute input_v3 validate">
                                    <label for="icon_ed_institute_1">Institute</label>
                                </div>
                                <div class="input-field col s6">
                                    <i class="fas fa-medal prefix"></i>
                                    <input id="icon_ed_degree_1" type="tel" class="icon_ed_degree input_v3 validate">
                                    <label for="icon_ed_degree_1">Degree</label>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="col s6">
                                    <div class="input-field col s12">
                                        <select id="icon_ed_study_type_1" class="icon_ed_study_type input_v3">
                                            <option value="" disabled selected>Choose your option</option>
                                            <option value="High School">High School</option>
                                            <option value="Associate degree">Associate degree</option>
                                            <option value="Bachelor">Bachelor</option>
                                            <option value="Master">Master</option>
                                            <option value="Doctorate">Doctorate</option>
                                        </select>
                                        <label>Study type</label>
                                    </div>
                                </div>
                                <div class="input-field col s6">
                                    <i class="fas fa-graduation-cap prefix"></i>
                                    <input id="icon_ed_score_1" type="tel" class="icon_ed_score input_v3 validate">
                                    <label for="icon_ed_score_1">Score</label>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div style="display: flex; justify-content: flex-start; align-items: center;">
                        <a id="education-add" class="btn-floating btn-large waves-effect waves-light btn-small">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="row" style="margin-bottom: 0;">
                        <button class="btn lastInfo waves-effect waves-light indigo lighten-4">Return
                            <i class="fas fa-arrow-left left"></i>
                        </button>
                        <button class="btn nextInfo waves-effect waves-light" style="background-color: #1D4289">Continue
                            <i class="fas fa-arrow-right right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div id="fill-info-4" style="display: none">
                <h1 id="edit-title" class="title">
                    Skills
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="col s12">
                    <div id="skills-container" style="margin-top: 20px;">
                        @if($cv->skills)
                            @foreach($cv->skills as $key => $skill)
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="input-field col s6">
                                        <i class="fab fa-python prefix"></i>
                                        <input id="icon_skill_name_{{$key}}" type="text" class="icon_skill_name input_v4 validate"
                                            @if($skill['name']) value="{{ $skill['name'] }}" @endif>
                                        <label for="icon_skill_name_{{$key}}">Name</label>
                                    </div>
                                    <div class="col s6">
                                        <div class="input-field col s12">
                                            <select id="icon_skill_level_{{$key}}" class="icon_skill_level input_v4">
                                                <option value="" disabled @if(!$skill['level']) selected @endif>Choose your option</option>
                                                <option value="Apprentice" @if($skill['level'] && $skill['level']=='Apprentice') selected @endif>Apprentice</option>
                                                <option value="Professional" @if($skill['level'] && $skill['level']=='Professional') selected @endif>Professional</option>
                                                <option value="Expert" @if($skill['level'] && $skill['level']=='Expert') selected @endif>Expert</option>
                                                <option value="Senior" @if($skill['level'] && $skill['level']=='Senior') selected @endif>Senior</option>
                                            </select>
                                            <label>Level</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else  
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="input-field col s6">
                                    <i class="fab fa-python prefix"></i>
                                    <input id="icon_skill_name_1" type="text" class="icon_skill_name input_v4 validate">
                                    <label for="icon_skill_name_1">Name</label>
                                </div>
                                <div class="col s6">
                                    <div class="input-field col s12">
                                        <select id="icon_skill_level_1" class="icon_skill_level input_v4">
                                            <option value="" disabled selected>Choose your option</option>
                                            <option value="Apprentice">Apprentice</option>
                                            <option value="Professional">Professional</option>
                                            <option value="Expert">Expert</option>
                                            <option value="Senior">Senior</option>
                                        </select>
                                        <label>Level</label>
                                    </div>
                                </div>
                            </div>  
                        @endif
                    </div>
                    <div style="display: flex; justify-content: flex-start; align-items: center;">
                        <a id="skills-add" class="btn-floating btn-large waves-effect waves-light btn-small">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="row" style="margin-bottom: 0;">
                        <button class="btn lastInfo waves-effect waves-light indigo lighten-4">Return
                            <i class="fas fa-arrow-left left"></i>
                        </button>
                        <button class="btn nextInfo waves-effect waves-light" style="background-color: #1D4289">Continue
                            <i class="fas fa-arrow-right right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div id="fill-info-5" style="display: none">
                <h1 id="edit-title" class="title">
                    Languages
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="col s12">
                    <div id="language-container" style="margin-top: 20px;">
                        @if($cv->languages)
                            @foreach($cv->languages as $key => $language)
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="input-field col s6">
                                        <i class="fas fa-language prefix"></i>
                                        <input id="icon_language_name_{{$key}}" type="text" class="icon_language_name input_v5 validate"
                                            @if($language['name']) value="{{ $language['name'] }}" @endif>
                                        <label for="icon_language_name_{{$key}}">Language</label>
                                    </div>
                                    <div class="col s6">
                                        <div class="input-field col s12">
                                            <select id="icon_language_level_{{$key}}" class="icon_language_level input_v5">
                                                <option value="" disabled @if(!$language['level']) selected @endif>Choose your option</option>
                                                <option value="Basic" @if($language['level'] && $language['level']=='Basic') selected @endif>Basic</option>
                                                <option value="Advanced" @if($language['level'] && $language['level']=='Advanced') selected @endif>Advanced</option>
                                                <option value="Expert" @if($language['level'] && $language['level']=='Expert') selected @endif>Expert</option>
                                                <option value="Native" @if($language['level'] && $language['level']=='Native') selected @endif>Native</option>
                                            </select>
                                            <label>Level</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else  
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="input-field col s6">
                                    <i class="fas fa-language prefix"></i>
                                    <input id="icon_language_name_1" type="text" class="icon_language_name input_v5 validate">
                                    <label for="icon_language_name_1">Language</label>
                                </div>
                                <div class="col s6">
                                    <div class="input-field col s12">
                                        <select id="icon_language_level_1" class="icon_language_level input_v5">
                                            <option value="" disabled selected>Choose your option</option>
                                            <option value="Basic">Basic</option>
                                            <option value="Advanced">Advanced</option>
                                            <option value="Expert">Expert</option>
                                            <option value="Native">Native</option>
                                        </select>
                                        <label>Level</label>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div style="display: flex; justify-content: flex-start; align-items: center;">
                        <a id="language-add" class="btn-floating btn-large waves-effect waves-light btn-small">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="row" style="margin-bottom: 0;">
                        <button class="btn lastInfo waves-effect waves-light indigo lighten-4">Return
                            <i class="fas fa-arrow-left left"></i>
                        </button>
                        <button class="btn nextInfo waves-effect waves-light" style="background-color: #1D4289">Continue
                            <i class="fas fa-arrow-right right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div id="fill-info-6" style="display: none">
                <h1 id="edit-title" class="title">
                    Main Projects
                </h1>
                <div class="cv-flex-cc">
                    <div style="width: 60%; height: 5px;
                        background-color: #363636; border-radius: 25px;"></div>
                </div>
                <div class="col s12">
                    <div id="project-container" style="margin-top: 20px;">
                        @if($cv->main_projects)
                            @foreach($cv->main_projects as $key => $project)
                                <div class="row" style="margin-bottom: 0px;">
                                    <div class="input-field col s6">
                                        <i class="fas fa-project prefix"></i>
                                        <input id="icon_project_name" type="text" class="icon_project_name input_v6 validate"
                                            @if($project['name']) value="{{ $project['name'] }}" @endif>
                                        <label for="icon_project_name">Name</label>
                                    </div>
                                    <div class="col s6">
                                        <div class="row" style="margin-bottom: 0px;">
                                            <div class="input-field col s12">
                                                <i class="fas fa-edit prefix"></i>
                                                <textarea id="icon_project_description" 
                                                    class="icon_project_description input_v6 materialize-textarea" data-length="280" maxlength="280">@if($project['description']) {{ $project['description'] }}@endif</textarea>
                                                <label for="icon_project_description">Description</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else  
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="input-field col s6">
                                    <i class="fas fa-project prefix"></i>
                                    <input id="icon_project_name_1" type="text" class="icon_project_name input_v6 validate">
                                    <label for="icon_project_name_1">Name</label>
                                </div>
                                <div class="col s6">
                                    <div class="row" style="margin-bottom: 0px;">
                                        <div class="input-field col s12">
                                            <i class="fas fa-edit prefix"></i>
                                            <textarea id="icon_project_description_1" 
                                                class="icon_project_description input_v6 materialize-textarea" data-length="280" maxlength="280"></textarea>
                                            <label for="icon_project_description_1">Description</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div style="display: flex; justify-content: flex-start; align-items: center;">
                        <a id="project-add" class="btn-floating btn-large waves-effect waves-light btn-small">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="row" style="margin-bottom: 0;">
                        <button class="btn lastInfo waves-effect waves-light indigo lighten-4">Return
                            <i class="fas fa-arrow-left left"></i>
                        </button>
                        <button id="finish-btn" class="btn waves-effect waves-light">Finish
                            <i class="fas fa-arrow-right right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 36px">
            <p id="phrases">Contact info is important, so enterprises can find you!</p>
        </div>
        <div class="cv-flex-cc">
            <progress id="advance-tool" class="progress is-small" value="0" max="100">0%</progress>
            <div id="advance-dot-1" class="advance-dot" style="left: 17.5%;"></div>
            <div id="advance-dot-2" class="advance-dot"></div>
            <div id="advance-dot-3" class="advance-dot" style="left: 41%;"></div>
        </div>
    </div>
    <div id="pdf-area" style="display: flex; justify-content: center; align-items: center;">
        <div style="width: 80%; height: 100%; display: flex; justify-content: center; align-items: center; overflow: auto; background-color: white;">
            <iframe id="pdf-iframe" src="{{ route('tmp', $cv->template ? $cv->template : '1') }}" scrolling="yes" frameborder="0" 
                style="height: 100%; width: 100%; transform-origin: top left;"></iframe>
            <div style="position: absolute; bottom: 0; margin-bottom: 20px;">
                <a href="{{ route('cv.download', [$cv->id, $cv->template ? 'template'.$cv->template : 'template1']) }}" target="_blank" class="waves-effect waves-light btn-large" style="background-color: #1D4289">Download PDF!</a>
            </div>
            <div id="zoom-btn" style="position: absolute; top: 0; margin-top: 20px; left: 60%; 
                margin-left: 20px; background-color: #1D4289; width: 40px; height: 40px;
                display: flex; justify-content: center; align-items: center; color: white;
                border-radius: 50%; cursor: pointer; transition: all 0.5s">
                <i id="zoom-in" class="fas fa-search-plus"></i>
                <i id="zoom-out" class="fas fa-search-minus" style="display: none"></i>
            </div>
        </div>
    </div>
</div>


<script>

var zoom = false;

$("#zoom-btn").click(function () {
    if (zoom == false) {
        $("#zoom-out").css('display', 'block');
        $("#zoom-in").css('display', 'none');
        $("#pdf-iframe").css('transform', 'scale(2)');
    } else {
        $("#zoom-out").css('display', 'none');
        $("#zoom-in").css('display', 'block');
        $("#pdf-iframe").css('transform', 'scale(1)');
    }
    zoom = !zoom;
});

document.getElementById("fill-info").addEventListener("submit", function(event){
  event.preventDefault()
});

var file = document.getElementById("file");

$("#img-area").css({
    "background-image": @if($cv->photo) "url('{{ asset("$cv->photo") }}') @else "url('{{ Storage::url('users/profilepics/no-photo.jpg') }}') @endif"
});



file.onchange = function(){
    if (file.files.length > 0) {
        document.getElementById('filename').innerHTML = file.files[0].name;
        previewFile(file.files[0]);
    }
};

function previewFile(file) {
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = function() {
        $("#img-area").css({
            "background-image": "url('" + reader.result + "')"
        });
        $("#img-area").empty();
    }
}

var actualInfoIndex = 1;

var phrases = [
    'Contact info is important, so enterprises can find you!',
    'Work experience gives you the opportunity to apply your skills.',
    'Education is one of the basic needs of human being.',
    'Learn how to gain or improve your skills at any time in your career.',
    'Open Up a World of Job Opportunities.'
];

$(".nextInfo").click(function () {
    $("#fill-info-" + actualInfoIndex).slideUp();
    actualInfoIndex++;
    $("#fill-info-" + actualInfoIndex).slideDown();
    $("#advance-tool").val((actualInfoIndex-2) * 25);
    if ( actualInfoIndex < 6) {
        if ( actualInfoIndex < 4) {
            if ( actualInfoIndex == 1) {
                $("#advance-dot-1").css("background-color", "#dbdbdb");
                $("#advance-dot-2").css("background-color", "#dbdbdb");
                $("#advance-dot-3").css("background-color", "#dbdbdb");
            } else {
                $("#advance-dot-1").css("background-color", "#4a4a4a");
                $("#advance-dot-2").css("background-color", "#dbdbdb");
                $("#advance-dot-3").css("background-color", "#dbdbdb");
            }
        } else {
            $("#advance-dot-2").css("background-color", "#4a4a4a");
            $("#advance-dot-3").css("background-color", "#dbdbdb");
        }
    } else {
        $("#advance-dot-3").css("background-color", "#4a4a4a");
    }
    $("#phrases").text(phrases[actualInfoIndex-1]);
});

$(".lastInfo").click(function () {
    $("#fill-info-" + actualInfoIndex).slideUp();
    actualInfoIndex--;
    $("#fill-info-" + actualInfoIndex).slideDown();
    $("#advance-tool").val((actualInfoIndex-2) * 25);
    if ( actualInfoIndex < 6) {
        if ( actualInfoIndex < 4) {
            $("#advance-dot-1").css("background-color", "#4a4a4a");
            $("#advance-dot-2").css("background-color", "#dbdbdb");
            $("#advance-dot-3").css("background-color", "#dbdbdb");
        } else {
            $("#advance-dot-2").css("background-color", "#4a4a4a");
            $("#advance-dot-3").css("background-color", "#dbdbdb");
        }
    } else {
        $("#advance-dot-3").css("background-color", "#4a4a4a");
    }
    $("#phrases").text(phrases[actualInfoIndex-1]);
});

$(document).ready(function() {
    $('input#input_text, textarea#textarea1').characterCounter();
});

$(document).ready(function(){
    $('.datepicker').datepicker();
});

$(document).ready(function(){
    $('select').formSelect();
});

var contLaboral = 2;
var contEducation = 2;
var contSkills = 2;
var contLanguage = 2;
var contProject = 2;

$("#laboral-add").click(function () {
    var data = `
        <hr/>
        <div class="row" style="margin-bottom: 0px;">
            <div class="input-field col s6">
                <i class="fas fa-building prefix"></i>
                <input id="icon_ent_name_`+contLaboral+`" type="text" class="icon_ent_name input_v2 validate">
                <label for="icon_ent_name_`+contLaboral+`">Enterprise</label>
            </div>
            <div class="input-field col s6">
                <i class="fab fa-black-tie prefix"></i>
                <input id="icon_ent_job_title_`+contLaboral+`" type="tel" class="icon_ent_job_title input_v2 validate">
                <label for="icon_ent_job_title_`+contLaboral+`">Job Title</label>
            </div>
        </div>
        <div class="row" style="margin-bottom: 0px;">
            <div class="col s6">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="input-field col s12">
                        <i class="fas fa-edit prefix"></i>
                        <textarea id="icon_ent_description_`+contLaboral+`" class="icon_ent_description input_v2 materialize-textarea" data-length="280" maxlength="280"></textarea>
                        <label for="icon_ent_description_`+contLaboral+`">Description</label>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 0px; width: 100%;">
                <div class="input-field col s2">
                    <input id="icon_ent_start_`+contLaboral+`" type="text" class="icon_ent_start input_v2 datepicker">
                    <label for="icon_ent_start_`+contLaboral+`">Start Date</label>
                </div>
                <div class="input-field col s2">
                    <input id="icon_ent_end_`+contLaboral+`" type="text" class="icon_ent_end input_v2 datepicker">
                    <label for="icon_ent_end_`+contLaboral+`">End Date</label>
                </div>
            </div>
        </div>
    `;
    contLaboral++;
    $("#laboral-container").append(data);
    $('.datepicker').datepicker();
});

$("#education-add").click(function () {
    data = `
        <hr/>
        <div class="row" style="margin-bottom: 0px;">
            <div class="input-field col s6">
                <i class="fas fa-university prefix"></i>
                <input id="icon_ed_institute_`+contEducation+`" type="text" class="icon_ed_institute input_v3 validate">
                <label for="icon_ed_institute_`+contEducation+`">Institute</label>
            </div>
            <div class="input-field col s6">
                <i class="fas fa-medal prefix"></i>
                <input id="icon_ed_degree_`+contEducation+`" type="tel" class="icon_ed_degree input_v3 validate">
                <label for="icon_ed_degree_`+contEducation+`">Degree</label>
            </div>
        </div>
        <div class="row" style="margin-bottom: 0px;">
            <div class="col s6">
                <div class="input-field col s`+contEducation+`2">
                    <select id="icon_ed_study_type_`+contEducation+`" class="icon_ed_study_type input_v3">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="High School">High School</option>
                        <option value="Associate degree">Associate degree</option>
                        <option value="Bachelor">Bachelor</option>
                        <option value="Master">Master</option>
                        <option value="Doctorate">Doctorate</option>
                    </select>
                    <label>Study type</label>
                </div>
            </div>
            <div class="input-field col s6">
                <i class="fas fa-graduation-cap prefix"></i>
                <input id="icon_ed_score_`+contEducation+`" type="tel" class="icon_ed_score input_v3 validate">
                <label for="icon_ed_score_`+contEducation+`">Score</label>
            </div>
        </div>
    `;
    $("#education-container").append(data);
    setTimeout(() => {
        $('select').formSelect();
    }, 500);
});

$("#skills-add").click(function () {
    data = `
        <div class="row" style="margin-bottom: 0px;">
            <div class="input-field col s6">
                <i class="fab fa-python prefix"></i>
                <input id="icon_skill_name_`+contSkills+`" type="text" class="icon_skill_name input_v4 validate">
                <label for="icon_skill_name_`+contSkills+`">Name</label>
            </div>
            <div class="col s6">
                <div class="input-field col s12">
                    <select id="icon_skill_level_`+contSkills+`" class="icon_skill_level input_v4">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="Apprentice">Apprentice</option>
                        <option value="Professional">Professional</option>
                        <option value="Expert">Expert</option>
                        <option value="Senior">Senior</option>
                    </select>
                    <label>Level</label>
                </div>
            </div>
        </div>  
    `;

    $("#skills-container").append(data);
    setTimeout(() => {
        $('select').formSelect();
    }, 500);
});

$("#language-add").click(function () {
    data = `
        <div class="row" style="margin-bottom: 0px;">
            <div class="input-field col s6">
                <i class="fas fa-language prefix"></i>
                <input id="icon_language_name_`+contLanguage+`" type="text" class="icon_language_name input_v5 validate">
                <label for="icon_language_name_`+contLanguage+`">Language</label>
            </div>
            <div class="col s6">
                <div class="input-field col s12">
                    <select id="icon_language_level_`+contLanguage+`" class="icon_language_level input_v5">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="Basic">Basic</option>
                        <option value="Advanced">Advanced</option>
                        <option value="Expert">Expert</option>
                        <option value="Native">Native</option>
                    </select>
                    <label>Level</label>
                </div>
            </div>
        </div>
    `;

    $("#language-container").append(data);
    setTimeout(() => {
        $('select').formSelect();
    }, 500);
});

$("#project-add").click(function () {
    data = `
        <hr />
        <div class="row" style="margin-bottom: 0px;">
            <div class="input-field col s6">
                <i class="fas fa-project prefix"></i>
                <input id="icon_project_name_`+contProject+`" type="text" class="icon_project_name input_v6 validate">
                <label for="icon_project_name_`+contProject+`">Name</label>
            </div>
            <div class="col s6">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="input-field col s12">
                        <i class="fas fa-edit prefix"></i>
                        <textarea id="icon_project_description_`+contProject+`" 
                            class="icon_project_description input_v6 materialize-textarea" data-length="280" maxlength="280"></textarea>
                        <label for="icon_project_description_`+contProject+`">Description</label>
                    </div>
                </div>
            </div>
        </div>
    `;

    $("#project-container").append(data);
});

$(".input_v1").change(function () {
    updateValsP1();
});

function updateValsP1() {
    var first_name = $("#icon_firstname").val();
    var last_name = $("#icon_lastname").val();
    var photo = $("#icon_lastname").val();
    var email = $("#icon_email").val();
    var telephone = $("#icon_telephone").val();
    var facebook = $("#icon_facebook").val();
    var github = $("#icon_github").val();
    var linkedin = $("#icon_linkedin").val();

    var formData = new FormData();

    formData.append("first_name", first_name);
    formData.append("last_name", last_name);
    formData.append("photo", photo);
    formData.append("email", email);
    formData.append("telephone", telephone);
    formData.append("facebook", facebook);
    formData.append("github", github);
    formData.append("linkedin", linkedin);

    jQuery.each(jQuery('#file')[0].files, function(i, file) {
        formData.append('image[]', file);
    });

    $.ajax({
        url: "{{ route('cv.edit', $cv->id) }}",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var iframe = document.getElementById("pdf-iframe");
            iframe.src = iframe.src;
        }
    });
}

$(document).on("change",".input_v2", function() {
    var formData = new FormData();

    $('.icon_ent_name').each(function(i, obj) {
        formData.append('enterprise_name[]', $(this).val());
    });
    $('.icon_ent_job_title').each(function(i, obj) {
        formData.append('enterprise_job_title[]', $(this).val());
    });
    $('.icon_ent_description').each(function(i, obj) {
        formData.append('enterprise_description[]', $(this).val());
    });
    $('.icon_ent_start').each(function(i, obj) {
        formData.append('enterprise_start[]', $(this).val());
    });
    $('.icon_ent_end').each(function(i, obj) {
        formData.append('enterprise_end[]', $(this).val());
    });

    $.ajax({
        url: "{{ route('cv.editcvjson1', $cv->id) }}",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var iframe = document.getElementById("pdf-iframe");
            iframe.src = iframe.src;
        }
    });
});

$(document).on("change",".input_v3", function() {
    var formData = new FormData();

    $('.icon_ed_institute').each(function(i, obj) {
        formData.append('education_institute[]', $(this).val());
    });
    $('.icon_ed_degree').each(function(i, obj) {
        formData.append('education_degree[]', $(this).val());
    });
    $('.icon_ed_study_type').each(function(i, obj) {
        formData.append('education_study_type[]', $(this).val());
    });
    $('.icon_ed_score').each(function(i, obj) {
        formData.append('education_score[]', $(this).val());
    });

    $.ajax({
        url: "{{ route('cv.editcvjson2', $cv->id) }}",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var iframe = document.getElementById("pdf-iframe");
            iframe.src = iframe.src;
        }
    });
});

$(document).on("change",".input_v4", function() {
    var formData = new FormData();

    $('.icon_skill_name').each(function(i, obj) {
        formData.append('skill_name[]', $(this).val());
    });
    $('.icon_skill_level').each(function(i, obj) {
        formData.append('skill_level[]', $(this).val());
    });

    $.ajax({
        url: "{{ route('cv.editcvjson3', $cv->id) }}",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var iframe = document.getElementById("pdf-iframe");
            iframe.src = iframe.src;
        }
    });
});

$(document).on("change",".input_v5", function() {
    var formData = new FormData();

    $('.icon_language_name').each(function(i, obj) {
        formData.append('language_name[]', $(this).val());
    });
    $('.icon_language_level').each(function(i, obj) {
        formData.append('language_level[]', $(this).val());
    });

    $.ajax({
        url: "{{ route('cv.editcvjson4', $cv->id) }}",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var iframe = document.getElementById("pdf-iframe");
            iframe.src = iframe.src;
        }
    });
});

$(document).on("change",".input_v6", function() {
    var formData = new FormData();

    $('.icon_project_name').each(function(i, obj) {
        formData.append('project_name[]', $(this).val());
    });
    $('.icon_project_description').each(function(i, obj) {
        formData.append('project_description[]', $(this).val());
    });

    $.ajax({
        url: "{{ route('cv.editcvjson5', $cv->id) }}",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var iframe = document.getElementById("pdf-iframe");
            iframe.src = iframe.src;
        }
    });
});

$("#finish-btn").click(function () {
    window.location.href = "{{ route('cv.view', $cv->id) }}";
});

</script>


@endsection

@php $cv = session('curriculum_'.session('actCV')) @endphp
<body>
    <table style="width:100%; text-align: left;">
        <tr>
            <th style="text-align: center; font-size: 200%;" colspan="2">{{ $cv->first_name }} {{ $cv->last_name }} </th>
        </tr>
        <tr>
        @if($cv->email)
            <td>Email: {{ $cv->email }} </td>
        @endif
        @if($cv->linkedin)
            <td>LinkedIn: {{ $cv->linkedin }} </td>
        @endif
        </tr>
        <tr>
        @if($cv->telephone)
            <td>Telephone: {{ $cv->telephone }} </td>
        @endif
        @if($cv->facebook)
            <td>Facebook: {{ $cv->facebook }} </td>
        @endif
        </tr>
        @if($cv->github)
        <tr>
            <td>GitHub: {{ $cv->github }} </td>
        </tr>
        @endif
        <br>
        <br>
    </table>
    <hr>
    @if($cv->professional_experience)
        @foreach($cv->professional_experience as $enterprise)
        <table style="width:100%; text-align: left;">
            @if($enterprise['job_title'])
            <tr>
                <th style="width: 30%;"><span style="color: red;">~</span> Working Experience: </th>
                <td>Job title: {{ $enterprise['job_title'] }} </td>
            </tr>
            @endif
            @if($enterprise['name'])
            <tr>
                <td style="width: 30%;"></td>
                <td>Enterprise: {{ $enterprise['name'] }} </td>
            </tr>
            @endif
            @if($enterprise['description'])
            <tr>
                <td style="width: 30%;"></td>
                <td>Description: {{ $enterprise['description'] }} </td>
            </tr>
            @endif
            @if($enterprise['start'])
            <tr>
                <td style="width: 30%;"></td>
                <td>Start date: {{ $enterprise['start'] }} </td>
            </tr>
            @endif
            @if($enterprise['end'])
            <tr>
                <td style="width: 30%;"></td>
                <td>End Date: {{ $enterprise['end'] }} </td>
            </tr>
            @endif
            <br>
        </table>
        @endforeach
    @endif
    <hr>
    @if($cv->education)
        @foreach($cv->education as $education)
            <table style="width:100%; text-align: left;">
                @if($education['institute'])
                <tr>
                    <th style="width: 30%;"><span style="color: red;">~</span> Education: </th>
                    <td>Institute: {{ $education['institute'] }} </td>
                </tr>
                @endif
                @if($education['degree'])
                <tr>
                    <td style="width: 30%;"></td>
                    <td>Degree: {{ $education['degree'] }} </td>
                </tr>
                @endif
                @if($education['study_type'])
                <tr>
                    <td style="width: 30%;"></td>
                    <td>Study type: {{ $education['study_type'] }} </td>
                </tr>
                @endif
                @if($education['score'])
                <tr>
                    <td style="width: 30%;"></td>
                    <td>Average score: {{ $education['score'] }} </td>
                </tr>
                @endif
                <br>
            </table>
        @endforeach
    @endif
    <hr>
    @if($cv->skills)
        @foreach($cv->skills as $skill)
            <table style="width:100%; text-align: left;">
                @if($skill['name'])
                <tr>
                    <th style="width: 30%;"><span style="color: red;">~</span> Skills: </th>
                    <td>Skill: {{ $skill['name'] }} </td>
                </tr>
                @endif
                @if($skill['level'])
                <tr>
                    <td style="width: 30%;"></td>
                    <td>Level: {{ $skill['level'] }} </td>
                </tr>
                @endif
                <br>
            </table>
        @endforeach
    @endif
    <hr>
    @if($cv->languages)
        @foreach($cv->languages as $language)
            <table style="width:100%; text-align: left;">
                @if($language['name'])
                <tr>
                    <th style="width: 30%;"><span style="color: red;">~</span> Languages: </th>
                    <td>Language: {{ $language['name'] }} </td>
                </tr>
                @endif
                @if($language['level'])
                <tr>
                    <td style="width: 30%;"></td>
                    <td>Level: {{ $language['level'] }} </td>
                </tr>
                @endif
                <br>
            </table>
        @endforeach
    @endif
    <hr>
    @if($cv->main_projects)
        @foreach($cv->main_projects as $project)
            <table style="width:100%; text-align: left;">
                @if($project['name'])
                <tr>
                    <th style="width: 30%;"><span style="color: red;">~</span> Projects: </th>
                    <td>Name: {{ $project['name'] }} </td>
                </tr>
                @endif
                @if($project['description'])
                <tr>
                    <td style="width: 30%;"></td>
                    <td>Description: {{ $project['description'] }} </td>
                </tr>
                @endif
                <br>
            </table>
        @endforeach
    @endif
</body>
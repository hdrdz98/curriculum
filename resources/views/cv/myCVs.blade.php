@extends('../templates/main')

@section('styles')
<style>

#edit-title {
    font-size: 3rem;
    font-family: 'Alata', sans-serif;
}

#main-subtitle {
    font-size: 1.5rem;
}

#fillable-area {
    height: calc(100vh - 128px);
    background-color: white;
    width: 60%;
}

#pdf-area {
    height: calc(100vh - 128px);
    background-color: #363636;
    width: 40%;
}

#advance-tool {
    width: 40%;
}

.advance-dot {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    position: absolute;
    background-color: #dbdbdb/*#4a4a4a*/;
    bottom: 2%;
}

.datepicker-modal {
    height: 350px;
}

</style>
@endsection

@section('content')

<h1 id="edit-title" class="title">
    Edit my CVs
</h1>

<div class="columns cb-flex-cc" style="justify-content: center;">
    @foreach($cvs as $key => $cv)
        <div class="card column" style="width: 300px; max-width: 300px; margin: 20px;">
            <div class="card-image">
                <iframe id="pdf-iframe" src="{{ route('tmp', $cv->template ? $cv->template : '1') }}" scrolling="yes" frameborder="0" 
                    style="height: 100%; width: 100%; transform-origin: top left;"></iframe>
            </div>
            <div class="card-content indigo lighten-5" 
                style="text-align: left; font-size: 14px; margin-top: 30px;">
                <p>ID: {{ $cv->id }}</p>
                <p>Created at: {{ $cv->created_at }}</p>
                <p>Updated at: {{ $cv->updated_at }}</p>
            </div>
            <div class="card-action">
                <a class="waves-effect waves-light btn pink lighten-1" href="{{ route('cv.delete', $cv->id) }}">Delete</a>
                <a class="waves-effect waves-light btn indigo lighten-2" href="{{ route('cv.edit.form', $cv->id) }}">Edit!</a>
            </div>
        </div>
        @if (($key+1) % 3 == 0)
        </div>
        <div class="columns cb-flex-cc" style="justify-content: center;">
        @endif
    @endforeach
</div>

@endsection
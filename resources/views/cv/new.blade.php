@extends('../templates/main')

@section('styles')
<style>

#main-title {
    font-size: 3rem;
    font-family: 'Acme', sans-serif;
    color: #363636;
}

#main-subtitle {
    font-size: 1.5rem;
    color: #363636;
}

</style>
@endsection

@section('content')

<section class="hero is-primary box" style="background-color: rgba(0,0,0,0.15);">
  <div class="hero-body" style="padding:0; margin:0;">
    <div class="container">
      <h1 class="title" id="main-title">
        Create your curriculum!
      </h1>
      <h2 class="subtitle" id="main-subtitle">
        Choose a format...
      </h2>
    </div>
  </div>
</section>

<div class="carousel">
  <a class="carousel-item" href="{{ route('cv.edit.form.temp', [$cv->id, '1']) }}" style="bottom: 60%; !important; top: auto;"><img src="{{ asset('resources/1.png') }}"></a>
  <a class="carousel-item" href="{{ route('cv.edit.form.temp', [$cv->id, '2']) }}" style="bottom: 60%; !important; top: auto;"><img src="{{ asset('resources/2.png') }}"></a>
  <a class="carousel-item" href="{{ route('cv.edit.form.temp', [$cv->id, '3']) }}" style="bottom: 60%; !important; top: auto;"><img src="{{ asset('resources/3.png') }}"></a>
</div>

<script>

    $(document).ready(function(){
        $('.carousel').carousel({
            numVisible: 3,
            dist: 1,
            indicators: true,
            shift: 20
        });
        
    });

</script>

@endsection
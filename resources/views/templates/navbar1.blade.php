<style>
    
#floating-thing {
    background-color: #5c6bc0;
    width: 500px;
    height: 500px;
    position: absolute;
    right: 0;
    border-radius: 50%;
    transform: translate(500px, -500px);
    transition: all 0.5s;
}

.cv-hb {
    background-color: #363636;
    height: 5px;
    border-radius: 25px;
}

#main-logo {
    font-size: 2rem;
    font-family: 'Acme', sans-serif;
    color: #363636;
    font-weight: 600;
    line-height: 1.125;
}

#mini-menu-container {
    justify-content: center;
}

#mini-menu-container p {
    color: #363636;
    font-size: 12px;
    position: absolute;
    margin-top: 25px;
    height: 40px;
    cursor: pointer;
    opacity: 0;
    transform: translateX(45px);
}

#mini-menu {
    display: flex;
    width: 50px;
    flex-flow: column;
    align-items: flex-end;
    justify-content: center;
}

#mini-menu-container * {
    transition: all 0.5s;
}

#mini-menu-container:hover #cv-hb-2, #mini-menu-container:hover #cv-hb-3 {
    width: 100% !important;
}

#mini-menu-container:hover p {
    opacity: 1;
    transform: translateX(0px);
}

@media screen and (max-width: 1088px) {
    #main-logo {
        margin-left: 10px;
    }
}

.dropdown-content {
    transition: all 0.2s !important;
}

#user-data-container:hover {
    background-color: rgba(0, 0, 0, 0.15);
    border-radius: 6px;
}

.navbar-link:not(.is-arrowless)::after {
    border-color: #4a4a4a !important;
}

</style>

<nav class="navbar" style="box-shadow: none;">
    <div class="container">
        <div class="navbar-brand">
            <a href="{{ url('/') }}" id="main-logo" class="cv-flex-cc">
                LET'S CV!
            </a>
        </div>
        <div class="navbar-menu">
            <div id="mini-menu-container" class="navbar-end">
                @if (Auth::check())
                <div id="user-data-container" class="navbar-item">
                    <div class="cv-flex-cc" style="width: 64px; height: 64px;">
                        <div style="width: 60%; height: 60%;
                            background-image: url('{{ Auth::user()->photo }}');
                            background-size: cover; border-radius: 50%;">
                        </div>
                    </div>
                    <a class="dropdown-trigger navbar-link" data-target='dropdown1' 
                        style="background-color: transparent !important; color: #4a4a4a !important;">
                        {{ Auth::user()->email }}
                    </a>
                    <ul id='dropdown1' class='dropdown-content'>
                        <li><a href="{{ route('mycvs') }}">My CVs</a></li>
                        <li><a href="{{ route('cvs.new') }}">New</a></li>
                        <li><a href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </div>
                @else
                <div id="mini-menu">
                    <div id="cv-hb-1" class="cv-hb" style="width: 100%; margin-bottom: 4px;"></div>
                    <div id="cv-hb-2" class="cv-hb" style="width: 80%; margin-bottom: 4px;"></div>
                    <div id="cv-hb-3" class="cv-hb" style="width: 60%;"></div>
                </div>
                <p>CONTACT</p>
                @endif
            </div>
        </div>
    </div>
    <div id="floating-thing"></div>
</nav>

<script>
@if (Auth::check())
$('.dropdown-trigger').dropdown();
@else
var floatingThing = false;

$(function(){
    $(document).click(function(){  
        if (floatingThing) {
            $("#floating-thing").css('transform', 'translate(500px, -500px)');
            setTimeout(() => {
                floatingThing = false;
            }, 500);
        }
    });
});

$("#mini-menu-container").click(function () {
    $("#floating-thing").css('transform', 'translate(250px, -250px)');
    setTimeout(() => {
        floatingThing = true;
    }, 500);
});
@endif

</script>
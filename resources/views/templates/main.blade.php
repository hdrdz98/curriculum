<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Curriculum</title>

    <!--link rel="shortcut icon" href="{{ asset('resources/logo.jpg') }}">
    <link rel="favicon" href="{{ asset('resources/logo.jpg') }}"-->

    <!-- Compiled and minified CSS -->
    <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bulma.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" 
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Compiled and minified JavaScript -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <link href="{{ asset('js/app.js') }}" rel="stylesheet">
    
    @yield('styles')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</head>
<body>

@if (session('alert'))
<div class="notification is-link" style="margin-bottom: 0; z-index: 100">
    <button class="delete"></button>
    {{ session('alert') }}
</div>
@endif

@if (session('error'))
<div class="notification is-danger" style="margin-bottom: 0; z-index: 100">
    <button class="delete"></button>
    {{ session('error') }}
</div>
@endif

<section class="hero is-fullheight wrapper">
<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
    <div class="hero-head" style="padding: 0px; z-index: 50">
        @include('templates.navbar1')
    </div>
    <div class="hero-body" style="padding: 0px; z-index: 30">
        <div class="container has-text-centered" style="z-index: 30">
            @yield('content')
        </div>
    </div>
</section>
<script>
$(document).on('click', '.delete', function(){ 
    $(this).parent().remove()
}); 
</script>
</body>
</html>
@extends('templates/main')
@section('styles')
<style>

#login-title {
    font-size: 2.5rem;
    font-family: 'Acme', sans-serif;
    color: #363636;
}

#login-subtitle {
    font-size: 1rem;
    color: #363636;
}

/* label color */
.input-field label {
    color: #363636;
}

/* label focus color */
.input-field input[type=text]:focus + label {
    color: #363636 !important;
}

/* label focus color */
.input-field input[type=password]:focus + label {
    color: #363636 !important;
}

/* label underline focus color */
.input-field input[type=text]:focus {
    border-bottom: 1px solid #363636 !important;
    box-shadow: 0 1px 0 0 #000;
}

/* label underline focus color */
.input-field input[type=password]:focus {
    border-bottom: 1px solid #363636 !important;
    box-shadow: 0 1px 0 0 #000;
}

/* valid color */
.input-field input[type=text].valid {
    border-bottom: 1px solid #363636;
    box-shadow: 0 1px 0 0 #363636;
}

</style>
@endsection
@section('content')

    <div class="columns is-variable is-vcentered is-centered is-1-mobile is-0-tablet is-3-desktop is-8-widescreen is-2-fullhd is-multiline level-item">
        <div class="column is-3 has-text-centered box" style="background-color: rgba(0, 0, 0, 0.15)">
            <div class="tiles field">
                <p class="title" id="login-title">Login</p>
                <div>
                    <div class="input-field">
                        <input id="Nombre" type="text" class="validate">
                        <label class="active" for="Nombre">Usuario</label>
                    </div>
                </div>
                <div>
                    <div class="input-field">
                        <input id="Pass" type="password" class="validate">
                        <label class="active" for="Pass">Contraseña</label>
                        <span class="helper-text">Forgot your password?</span>
                    </div>
                </div>
                <div class="row">
                    <a class="waves-effect waves-light btn indigo lighten-1">Sign in</a>
                </div>
                <label class="label" id="login-subtitle">Or sign up with</label>
                <div class="row">
                    <a href="{{ route('github.login') }}" class="btn-floating btn-large waves-effect waves-light btn-small grey darken-3"> <i class="fab fa-github"></i> </a>
                </div>
            </div>
        </div>
    </div>



@endsection
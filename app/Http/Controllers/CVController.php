<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Curriculum;
use PDF;

class CVController extends Controller
{
    public function viewCV($id)
    {
        $cv = Curriculum::findOrFail($id);
        $params = ['cv' => $cv];
        $num = $cv->template != [] ? $cv->template : '1';

        $pdf = PDF::loadView('cv.templates.template'.$num, $params);
        return $pdf->stream('curriculum.pdf');

        //return view('cv.templates.template'.$num, $params);
    }

    public function showNewForm($id) 
    {
        $cv = Curriculum::findOrFail($id);
        $params = ['cv' => $cv];
        return view('cv.new', $params);
    }

    public function showEditArea(Request $request, $id) 
    {
        $cv = Curriculum::findOrFail($id);
        $request->session()->put('curriculum_'.$id, $cv);
        $request->session()->put('actCV', $id);
        $user = Auth::user();
        $params = ['user' => $user, 'cv' => $cv];
        return view('cv.edit', $params);
    }

    public function showEditAreaWTemplate(Request $request, $id, $template) 
    {
        $cv = Curriculum::findOrFail($id);
        $cv->template = $template;
        $cv->update();
        $request->session()->put('curriculum_'.$id, $cv);
        $request->session()->put('actCV', $id);
        $user = Auth::user();
        $params = ['user' => $user, 'cv' => $cv];
        return view('cv.edit', $params);
    }

    public function myCVs()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $cvs = Curriculum::where('user_id', $user->id)->get();
            return view('cv.myCVs', ['cvs' => $cvs]);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function showCV($id)
    {
        $cv = Curriculum::findOrFail($id);
        $params = [
            'cv' => $cv,
        ];
        return view('cv.view', $params);
    }

    public function delete($id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $cv = Curriculum::findOrFail($id);
            if ($cv->user_id == $user->id){
                $cv->delete();
                return back()->with('alert', 'Successfully deleted the cv.');
            } else {
                return back()->with('error', 'You are not allowed to delete this cv.');
            }
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function editCV(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $curriculum = Curriculum::find($id);
            $curriculum->update($request->all());

            if ($request->image) {
                $imageNameExploted = explode('.', $request->image[0]->getClientOriginalName());
                $imageName = strtolower('image_'.$id).'.'.$imageNameExploted[count($imageNameExploted) - 1];
                $request->image[0]->storeAs('public/users/profilepics/', $imageName);
                $curriculum->photo = 'storage/users/profilepics/'.$imageName;
            }

            $curriculum->update();

            $request->session()->put('curriculum_'.$id, $curriculum);
            $request->session()->put('actCV', $id);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function editCVJson1(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $curriculum = Curriculum::find($id);
            
            $data = [];

            for ($i=0; $i < count($request->enterprise_name); $i++) { 
                array_push($data, [
                    'name' => $request->enterprise_name[$i],
                    'job_title' => $request->enterprise_job_title[$i],
                    'description' => $request->enterprise_description[$i],
                    'start' => $request->enterprise_start[$i],
                    'end' => $request->enterprise_end[$i]
                ]);
            }

            $curriculum->professional_experience = $data;

            $curriculum->update();

            $request->session()->put('curriculum_'.$id, $curriculum);
            $request->session()->put('actCV', $id);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function editCVJson2(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $curriculum = Curriculum::find($id);
            
            $data = [];

            for ($i=0; $i < count($request->education_institute); $i++) { 
                array_push($data, [
                    'institute' => $request->education_institute[$i],
                    'degree' => $request->education_degree[$i],
                    'study_type' => $request->education_study_type[$i],
                    'score' => $request->education_score[$i],
                ]);
            }

            $curriculum->education = $data;

            $curriculum->update();

            $request->session()->put('curriculum_'.$id, $curriculum);
            $request->session()->put('actCV', $id);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function editCVJson3(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $curriculum = Curriculum::find($id);
            
            $data = [];

            for ($i=0; $i < count($request->skill_name); $i++) { 
                array_push($data, [
                    'name' => $request->skill_name[$i],
                    'level' => $request->skill_level[$i]
                ]);
            }

            $curriculum->skills = $data;

            $curriculum->update();

            $request->session()->put('curriculum_'.$id, $curriculum);
            $request->session()->put('actCV', $id);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function editCVJson4(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $curriculum = Curriculum::find($id);
            
            $data = [];

            for ($i=0; $i < count($request->language_name); $i++) { 
                array_push($data, [
                    'name' => $request->language_name[$i],
                    'level' => $request->language_level[$i]
                ]);
            }

            $curriculum->languages = $data;

            $curriculum->update();

            $request->session()->put('curriculum_'.$id, $curriculum);
            $request->session()->put('actCV', $id);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function editCVJson5(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $curriculum = Curriculum::find($id);
            
            $data = [];

            for ($i=0; $i < count($request->project_name); $i++) { 
                array_push($data, [
                    'name' => $request->project_name[$i],
                    'description' => $request->project_description[$i]
                ]);
            }

            $curriculum->main_projects = $data;

            $curriculum->update();

            $request->session()->put('curriculum_'.$id, $curriculum);
            $request->session()->put('actCV', $id);
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function downloadCV(Request $request, $id, $template) {
        if (Auth::check()) {
            $user = Auth::user();
            $cv = Curriculum::find($id);
            $params = ['cv' => $cv];
            //$pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->
            $pdf = PDF::loadView('cv.templates.'.$template, $params); 
            return $pdf->download('curriculum.pdf');
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }

    public function createNewCVAndRedirect()
    {
        if (Auth::check()) {
            $cv = Curriculum::create([
                'user_id' => Auth::user()->id,
                'email' => Auth::user()->email
            ]);
            return redirect('cv/'.$cv->id.'/new');
        } else {
            return back()->with('error', 'You have to be logged in to use this function.');
        }
    }
}

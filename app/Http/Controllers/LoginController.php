<?php

namespace App\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Curriculum;

class LoginController extends Controller
{
    public function showForm()
    {
        if (Auth::check())
            return redirect($this->createNewCVAndRedirect());
        else
            return view('login');
    }

    public function redirectToGitHub()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleGitHubCallback()
    {
        $githubUser = Socialite::driver('github')->user();

        $appUser = User::firstOrCreate([
            'email' => $githubUser->getEmail()
        ], [
            'nickname' => $githubUser->getNickname(),
            'name' => $githubUser->getName(),
            'photo' => $githubUser->getAvatar(),
            'password' => Bcrypt('password')
        ]);
 
        auth()->login($appUser);

        return back()->with('alert', 'Inicio de sesión exitoso');
    }

    public function logout() 
    {
        Auth::logout();
        return redirect('/');
    }

    public function createNewCVAndRedirect()
    {
        if (Auth::check()) {
            $cv = Curriculum::create([
                'user_id' => Auth::user()->id,
                'email' => Auth::user()->email
            ]);
            return url('cv/'.$cv->id.'/new');
        } else {
            return url('/')->with('error', 'You have to be logged in to use this function.');
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'photo', 'email', 'template',
        'telephone', 'facebook', 'github', 'linkedin', 'professional_experience',
        'education', 'skills', 'languages', 'main_projects'
    ];

    protected $casts = [
        'professional_experience' => 'array',
        'education' => 'array',
        'skills' => 'array',
        'languages' => 'array',
        'main_projects' => 'array',
    ];
}
